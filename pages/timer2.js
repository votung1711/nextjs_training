import { Icon } from 'antd';
import { useState, useEffect } from 'react';
import { Row, Col, Button } from 'antd';
import { Layout } from 'antd';
import PageHead from '../components/header';
import Foot from '../components/footer';
import SideBar from '../components/sider';
// import '../styles/styles.css';
const { Content } = Layout;

const Timer2 = () => {
    const [hours, setHours] = useState(null);
    const [minutes, setMinutes] = useState(null);
    const [seconds, setSeconds] = useState(null);
    const [state, setState] = useState(false);
    const [resume, setResume] = useState(false);
    const [time, setTotalTime] = useState(null);
    const [inter, setInter] = useState(null);

    const formatTime = (time) => {
        return (time < 10 ? `0${time}`.toString().substring(0, 2) : `${time}`.toString().substring(0, 2));
    }

    const countdown = (duration) => {
        var milionSeconds = formatTime(parseInt((Math.floor(duration % 1000)))),
            seconds = Math.floor((duration / 1000) % 60),
            minutes = Math.floor((duration / (1000 * 60)) % 60),
            hours = Math.floor((duration / (1000 * 60 * 60)));
        hours = (hours < 10) ? `0${hours}` : hours;
        minutes = (minutes < 10) ? `0${minutes}` : minutes;
        seconds = (seconds < 10) ? `0${seconds}` : seconds;
        milionSeconds = (milionSeconds < 10) ? `0${milionSeconds}` : milionSeconds;
        if (duration <= 0) {
            return "00:00:00:00"
        }
        return `${hours}:${minutes}:${seconds}:${milionSeconds}`;
    }

    const startTime = () => {
        setState(!state);
        var totalTime = ((hours * 60 * 60 * 1000) + (minutes * 60 * 1000) + (seconds * 1000));
        setTotalTime(totalTime);
        setInter(setInterval(() => {
            setTotalTime(x => x - 10);
        }, 10))
    }

    const stopTime = () => {
        setResume(!resume);
        clearInterval(inter);
    }

    const resumeTime = () => {
        setResume(!resume);
        setInter(setInterval(() => {
            setTotalTime(x => x - 10);
        }, 10))
    }

    const resetTime = () => {
        setState(!state);
        console.log(resume);
        setResume(false);
        clearInterval(inter);
        setHours(null);
        setMinutes(null);
        setSeconds(null);

    }

    const clearBtn = (numb) => {
        switch (numb) {
            case '.':
                setHours(null);
                setMinutes(null);
                setSeconds(null);
                break;
            case 'x':
                if (minutes === null || minutes === '') {
                    setHours(hours.substring(0, hours.length - 1));
                } else if (seconds === null || seconds === '') {
                    setMinutes(minutes.substring(0, minutes.length - 1));
                } else if (seconds !== null || seconds !== '') {
                    setSeconds(seconds.substring(0, seconds.length - 1));
                }
                break;
            default:
        }
    }

    const setItem = (numb) => {
        if (numb === 'x' || numb === '.') {
            clearBtn(numb);
        }
        else if (hours === null) {
            setHours(numb);
        } else if (hours.length < 2 && numb !== 'x') {
            setHours(hours.concat(numb));
        } else if (minutes === null) {
            setMinutes(numb);
        } else if (minutes.length < 2 && numb !== 'x') {
            setMinutes(minutes.concat(numb));
        } else if (seconds === null) {
            setSeconds(numb);
        } else if (seconds.length < 2 && numb !== 'x') {
            setSeconds(seconds.concat(numb));
        }

    }

    const arrBtn = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'x', '0', '.'];
    const mapBtn = arrBtn.map((numb, index) => {
        return (
            <Col key={index} span={8}  >
                <Button onClick={() => setItem(numb)} style={{ width: '100%', height: '30px' }}>
                    {numb}
                </Button>
            </Col>
        )
    });

    return (
        <div>
            <Layout>
                <PageHead />
                <Layout>
                    <SideBar />
                    <Content style={{ margin: '24px 16px 0' }}>
                        <div style={{ padding: 24, background: '#fff', minHeight: 800 }}>
                            <div className="counter">
                                <div className="timer2">
                                    <Icon type="hourglass" style={{ fontSize: '36px', color: 'white', marginLeft: '20%' }} />
                                    <b>React Timer</b>
                                </div>
                                <div className="timer">
                                    {
                                        !state
                                            ?
                                            <div className="actionBtn">
                                                <Row style={{bottom: '50%'}}>
                                                    <Col style={{ textAlign: 'center', fontSize: 24 }} xs={{ span: 8 }} lg={{ span: 8 }}>
                                                        H
                                            </Col>
                                                    <Col style={{ textAlign: 'center', fontSize: 24 }} xs={{ span: 8 }} lg={{ span: 8 }}>
                                                        M
                                            </Col>
                                                    <Col style={{ textAlign: 'center', fontSize: 24 }} xs={{ span: 8 }} lg={{ span: 8 }}>
                                                        S
                                            </Col>
                                                </Row>
                                                <div>

                                                    <div className="inputText" style={{ marginTop: 15 }}>
                                                        <Row style={{ marginTop: '50px' }}>
                                                            <Col span={6} >
                                                                <input type="text" disabled defaultValue={hours} className="numb" />
                                                            </Col>
                                                            <Col span={3}  ><span className="dot">:</span></Col>
                                                            <Col span={6}  >
                                                                <input type="text" disabled defaultValue={minutes} className="numb" />
                                                            </Col>
                                                            <Col span={3} ><span className="dot">:</span></Col>
                                                            <Col span={6}  >
                                                                <input type="text" disabled defaultValue={seconds} className="numb" />
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                    <div className="number">
                                                        <Row>
                                                            {mapBtn}
                                                        </Row>
                                                    </div>
                                                </div>
                                                <div className="btnStart" onClick={() => startTime()}>
                                                    <div className="test">
                                                        <h5 className="textStart">Start</h5>
                                                    </div>
                                                </div>
                                            </div>

                                            :
                                            <div className="btnAction">
                                                <div className="countdownTime">
                                                    <span>{countdown(time)}</span>
                                                </div>
                                                {
                                                    !resume
                                                        ?
                                                        <div className="btnStop" onClick={() => stopTime()}>
                                                            <h5 className="textStop">Stop</h5>
                                                        </div>
                                                        :
                                                        <div className="btnResume" onClick={() => resumeTime()}>
                                                            <h5 className="textResume">Resume</h5>
                                                        </div>
                                                }
                                                <div className="btnReset" onClick={() => resetTime()}>
                                                    <h5 className="textReset">Reset</h5>
                                                </div>
                                            </div>

                                    }

                                </div>

                                <style jsx>{`
           .counter {
            left:32%;
            margin-top: 30px;
            position: relative;
            width: 350px;
            height: 450px;
            border: 2px solid black
        }
        
        .timer2 {
           width:100%;
           height: 40px;
           background-color: black
        }
        .timer2 {
           color: #e8e8e8
        }
        .timer2 b {
           float: right;
           margin: 5px 25% 5px 5px;
           font-size: 24px;   
        }
        .timer {
           width: 230px;
           height: 300px;
           border: 2px solid black;
           position: absolute;
           top: 20%;
           left: 16%;
           background-color: #f1f1f1;
        }
         .inputText {
             padding: -30px !important;
             padding-left: 20px;
         }
         .numb{
           top: 30%;
           font-size: 28px;
           color: black;
           font-weight: bold;
           text-align: center;
           left: 30%;
           width: 40px;
           height:40px;
           border:none;
            }
         .dot {
           font-size: 25px;
            }
           .number {
               width: 100%;
               margin-top: 60px;
           }
           .actionBtn {
               width:100%;
               height: 40px;
               position: relative;
           }
         .btnStart {
             position: absolute;
             cursor: pointer;
             width:100%;
             height: 40px;
             background-color: #52c41a;
             margin-top: 87%;
         }
         .btnStop {
           position: absolute;
           width: 50%;
           height: 40px;
           margin-top: 62%;
           background-color: #cf1322;
         }
         .btnReset{
           position: absolute;
           width: 50%;
           left: 50%;
           height: 40px;
           margin-top: 62%;
           background-color: #167ae5;
         }
         .btnResume {
           position: absolute;
           width: 50%;
           height: 40px;
           margin-top: 62%;
           background-color:  #52c41a;
         }
         .btnAction {
           position:relative;
           cursor: pointer;
           width:100%;
           height: 40px;
           top: 39%;
         }
         .test {
           width: 100%;
           position: relative;
         }
         .textStart {
             position: absolute;
             width:100%;
             font-size: 24px;
             text-transform: uppercase;
             color: white;
             text-align:center;
             margin-top: 8px
             
         }
         .textStop {
               position: absolute;
               font-size: 24px;
               text-transform: uppercase;
               color: white;
               top: 10%;
               left: 25%;
         }
         .textReset {
           position: absolute;
          font-size: 24px;
          text-transform: uppercase;
          color: white;
          top: 10%;
          left: 15%;
         }
         .textResume {
           position: absolute;
           font-size: 24px;
           text-transform: uppercase;
           color: white;
           top: 10%;
           left: 5%;
        }
         .countdownTime {
             position: absolute;
             width: 100%;
             height: 50px;
             left: 12%;
             top: 10%;
             bottom: 100%;
         }
         .countdownTime span{
           font-size: 32px;
           color: #167ae5;
           font-weight: bold;
         }
        
            `}
                                </style>
                            </div>
                        </div>
                    </Content>
                </Layout>
                <Foot />
            </Layout>
        </div>

    )
}
export default Timer2;