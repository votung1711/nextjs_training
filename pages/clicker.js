import React, { useState, useEffect } from 'react';
import { Icon, Button, Layout } from 'antd';
import PageHead from '../components/header';
import Foot from '../components/footer';
import SideBar from '../components/sider';

const {Content} = Layout;
const Clicker = () => {
    const [count, setCount] = useState(0);

    useEffect(() => {
        document.writeln = `${count}`;
        console.log(count);
    })
    return (
        <div>
            <Layout>
                <PageHead />
                <Layout>
                    <SideBar />
                    <Content style={{ margin: '24px 16px 0' }}>
                        <div style={{ padding: 24, background: '#fff', minHeight: 800 }}>
                            <div className="counter">
                                <div className="clicker">
                                    <Icon type="like" style={{ fontSize: '36px', color: 'white', marginLeft: '20%' }} />
                                    <b>React Clicker</b>
                                </div>
                                <div className="result">
                                    <div className="list">
                                        <p>{count}</p>
                                        <div className="btnEvent">
                                            <Button style={{backgroundColor: '#52c41a', width:80, height:60}} onClick={() => setCount(count + 1)}><Icon style={{ fontSize: '30px', marginLeft: '5%', color: 'white', marginTop: '5px' }} type="plus" /></Button>
                                            <Button style={{backgroundColor:'#faad14',  width:80, height:60}} onClick={() => setCount(0)}><Icon style={{ fontSize: '30px', marginLeft: '5%', color: 'white' }} type="sync" /></Button>
                                            <Button type="danger" style={{ width:80, height:60}} onClick={() => setCount(count - 1)}><Icon style={{ fontSize: '30px', marginLeft: '5%', color: 'white' }} type="minus" /></Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <style jsx>{`
                 .counter {
                    left:32%;
                    margin-top: 30px;
                     position: relative;
                     width: 350px;
                     height: 450px;
                     border: 2px solid black
              }
                .clicker {
                    width:100%;
                    height: 40px;
                    background-color: black
                }
                .clicker {
                    color: #e8e8e8
                }
                .clicker b {
                    float: right;
                    margin: 5px 15% 5px 5px;
                    font-size: 24px;
                    
                }
                .result {
                    height: 250px;
                    position: absolute;
                    left:15%;
                    top: 20%;
                    border: 4px solid #e8e8e8;
                }
                .list {
                    width: 240px;
                    height: 150px;
                    position: relative
                    
                }
                .result p {
                    text-align: center;
                    font-size: 60px;
                    color: #595959;
                    padding-top: 10%;
                }
                .btnEvent {
                    display: flex;
                    position: absolute;
                    bottom: -60%;
                }

        `}
                            </style>
                        </div>
                    </Content>
                </Layout>
                <Foot />
            </Layout>
        </div >
    )
}

export default Clicker;