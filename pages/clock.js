import { Icon, Button, Layout, Switch } from 'antd';
import PageHead from '../components/header';
import Foot from '../components/footer';
import SideBar from '../components/sider';
import { useState, useEffect } from 'react';
const { Content } = Layout;

const Clock = (id) => {
    const [time, setTime] = useState(new Date);
    const [state, setState] = useState(true);

    useEffect(() => {
        var timerID = setInterval(() => tick(), 1000);
    });

    function tick() {
        setTime(new Date());
    }
    const click = () => {
        setState(!state);
    }
    return (
        <div>
            <Layout>
                <PageHead />
                <Layout>
                    <SideBar />
                    <Content style={{ margin: '24px 16px 0' }}>
                        <div style={{ padding: 24, background: '#fff', minHeight: 800 }}>
                            <div className="counter">
                                <div className="timer2">
                                    <Icon type="clock-circle" style={{ fontSize: '36px', color: 'white', marginLeft: '20%' }} />
                                    <b>React Clock</b>
                                </div>
                                <div className="circle1">
                                    <div className="checkTime">
                                        <label className="switch">
                                            <input type="checkbox" onClick={() => click()} />
                                            <span className="slider round"></span>
                                        </label>
                                        {/* not show button checkbox switch */}
                                        {/* <Switch loading="true" onClick={() => click()} /> */}
                                        <div className="calendar">
                                            <Icon type="calendar" style={{ fontSize: '30px', color: '#08c' }} theme="filled" />
                                        </div>

                                    </div>
                                    <div className="timer">
                                        <h3 className="timeSystem">
                                            {time.toLocaleTimeString('it-IT')}
                                        </h3>
                                        <h4 className="dateTime">
                                            {
                                                !state ?
                                                    time.toLocaleString('en-DE', { weekday: 'short', year: 'numeric', month: 'long', day: '2-digit' }).split(",")
                                                    // 2-digit -> them so 0 vao truoc neu < 10
                                                    : ''
                                            }
                                        </h4>
                                    </div>
                                </div>
                                <style jsx>{`
                     .counter {
                        left:32%;
                        margin-top: 30px;
                         position: relative;
                         width: 350px;
                         height: 450px;
                         border: 2px solid black
                  }
                    .timer2 {
                        width:100%;
                        height: 40px;
                        background-color: black
                    }
                    .timer2 {
                        color: #e8e8e8
                    }
                    .timer2 b {
                        float: right;
                        margin: 5px 25% 5px 5px;
                        font-size: 24px;
                        
                    }
                    .circle1 {
                        position: relative;
                        margin-left:20%;
                        margin-top:20%;
                        width: 200px;
                        height:200px;
                        border: 4px solid #e8e8e8;
                        border-radius: 200px;
                        background-color: #4a5360;
                        display: flex;
                    }
                    .timer {
                        position: absolute;
                        margin-top: 3%; 
                        margin-left: 3%;
                        width: 180px;
                        height: 180px;
                        border: 1px solid #e8e8e8;
                        border-radius: 180px;
                        background-color: black;
                    }
                    .timeSystem{
                        position: absolute;
                        top: 16%;
                        left: 5%;
                        font-size: 40px;
                        color: #3b8ddb;
                    }
                    .dateTime {
                        position: absolute;
                        top: 50%;
                        left: 10%;
                        text-align: center;
                        font-size: 16px;
                        color: #3b8ddb;
                    }
                    .checkTime {
                        position:absolute;
                        margin-top: -30%;
                        margin-left: 20%;
                        display:flex;
                    }
                    .calendar {
                        margin-left: 5px;
                    }
                    .switch {
                        position: relative;
                        display: inline-block;
                        width: 45px;
                        height: 24px;
                        margin-top: 5px;
                      }
                      
                      .switch input { 
                        opacity: 0;
                        width: 0;
                        height: 0;
                      }
                      
                      .slider {
                        position: absolute;
                        cursor: pointer;
                        top: 0;
                        left: 0;
                        right: 0;
                        bottom: 0;
                        background-color: #ccc;
                        -webkit-transition: .4s;
                        transition: .4s;
                      }
                      
                      .slider:before {
                        position: absolute;
                        content: "";
                        height: 18px;
                        width: 18px;
                        left: 4px;
                        bottom: 3px;
                        background-color: white;
                        -webkit-transition: .4s;
                        transition: .4s;
                      }
                      
                      input:checked + .slider {
                        background-color: #2196F3;
                      }
                      
                      input:focus + .slider {
                        box-shadow: 0 0 1px #2196F3;
                      }
                      
                      input:checked + .slider:before {
                        -webkit-transform: translateX(26px);
                        -ms-transform: translateX(26px);
                        transform: translateX(20px);
                      }
                      
                      /* Rounded sliders */
                      .slider.round {
                        border-radius: 34px;
                      }
                      
                      .slider.round:before {
                        border-radius: 50%;
                      }
            `}
                                </style>
                            </div>
                        </div>
                    </Content>
                </Layout>
                <Foot />
            </Layout>
        </div>
    )
}


export default Clock;