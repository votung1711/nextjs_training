import { Icon } from 'antd';
import { useState } from 'react';
import { Layout } from 'antd';
import PageHead from '../components/header';
import Foot from '../components/footer';
import SideBar from '../components/sider';
const { Content } = Layout;

const Timer = () => {

    const [secs, resultTime] = useState(0)
    const [pause, isPause] = useState(false)
    const [count, setCount] = useState(0);

    function incrementSeconds() {
        changeStatus();
        if (!pause) {
            setCount(setInterval(() => {
                resultTime(time => time + 1);
            }, 1000));
        } else {
            clearInterval(count);

        }
    }

    const changeStatus = () => {
        isPause(!pause);
    }

    return (
        <div>
            <Layout>
                <PageHead />
                <Layout>
                <SideBar />
                    <Content style={{ margin: '24px 16px 0' }}>
                        <div style={{ padding: 24, background: '#fff', minHeight: 800 }}>
                            <div className="counter">
                                <div className="clicker">
                                    <Icon type="hourglass" style={{ fontSize: '36px', color: 'white', marginLeft: '20%' }} />
                                    <b>React Timer</b>
                                </div>
                                <div className="circle1">
                                    <input type="text" defaultValue={secs} className="numb" />
                                    <div className="timer">
                                        <svg onClick={() => resultTime(0)} className="reset" height="60" width="280">
                                            <circle cx="280" cy="100" r="100" fill="#096dd9" ></circle>
                                            <text className="btnReset" x="230" y="50">&#8635;</text>
                                        </svg>
                                        {
                                            !pause ?
                                                <svg onClick={() => incrementSeconds(secs)} className="btnRun" height="60" width="280">
                                                    <circle cx="280" cy="100" r="100" fill="#52c41a" ></circle>
                                                    <text className="run" x="230" y="50" >&#9654;</text>
                                                </svg>
                                                :
                                                <svg onClick={() => incrementSeconds(secs)} className="btnPause" height="60" width="280">
                                                    <circle cx="280" cy="100" r="100" fill="#black" ></circle>
                                                    <text className="pause" x="230" y="40" >&#9607;</text>
                                                </svg>
                                        }

                                    </div>
                                </div>
                                <style jsx>{`
                 .counter {
                    left:32%;
                    margin-top: 30px;
                     position: relative;
                     width: 350px;
                     height: 400px;
                     border: 2px solid black
              }
                .clicker {
                    width:100%;
                    height: 40px;
                    background-color: black
                }
                .clicker {
                    color: #e8e8e8
                }
                .clicker b {
                    float: right;
                    margin: 5px 25% 5px 5px;
                    font-size: 24px;
                    
                }
                .circle1 {
                    position: relative;
                    margin-left:20%;
                    margin-top:10%;
                    width: 200px;
                    height:200px;
                    border: 4px solid #e8e8e8;
                    border-radius: 200px;
                    background-color: #fafafa;
                    display: flex;
                }
                .numb {
                    position:absolute;
                    top: 30%;
                    font-size: 32px;
                    color: #096dd9;
                    text-align: center;
                    margin-left: 5px;
                    background-color: #fafafa;
                    left: 33%;
                    width: 60px;
                    height:50px;
                    border:none;
                }
                .numb:focus {
                    border: none;
                }
                .btnRun {
                    cursor: pointer;
                    position: absolute;
                    transform: rotateY(180deg)
                }
                .btnRun .run {
                    font-family: sans-serif;
                    font-size: 40px; 
                    fill: white;
                }
                .btnPause {
                    cursor: pointer;
                    position: absolute;
                    transform: rotateY(180deg)
                }
                .btnPause .pause {
                    position: absolute;
                    font-family: sans-serif;
                    font-size: 30px; 
                    fill: white;
                }
                .reset {
                    cursor: pointer;
                }
                .btnReset {
                    font-family: sans-serif;
                    font-size: 40px; 
                    fill: white;
                }
                .timer {
                    margin-left:50%;
                    transform: rotate(180deg)
                }
            
        `}
                                </style>
                            </div>
                        </div>
                    </Content>
                </Layout>
                <Foot />
            </Layout>
            
        </div>
    )
}

export default Timer;